using System.Text;

namespace BAS.DwhGenerator
{
    public static class SnowflakeFieldFormatter
    {
        public static string FormatField(Field field)
        {
            var fieldString = new StringBuilder(field.SnowflakeColumnName);

            switch (field.Type)
            {
                case FieldType.Boolean:
                    fieldString.Append(" boolean");
                    break;
                case FieldType.DateTime:
                    fieldString.Append(" datetime");
                    break;
                case FieldType.Decimal:
                    fieldString.Append($" decimal({field.NumericPrecision}, {field.NumericScale})");
                    break;
                case FieldType.Int:
                    fieldString.Append(" int");
                    break;
                case FieldType.String:
                    var stringSize = field.StringMaxLength;
                    if (stringSize == -1) stringSize = 16777216;
                    fieldString.Append($" varchar({stringSize})");
                    break;
            }

            fieldString.Append(field.IsNullable ? string.Empty : " not null");

            return fieldString.ToString();
        }
    }
}