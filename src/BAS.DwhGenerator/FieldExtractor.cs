﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace BAS.DwhGenerator
{
    public static class FieldExtractor
    {
        public static List<Field> ExtractFields(string databaseName, string schemaName, string tableName)
        {
            var connectionString = $"Data Source=shdb5;Initial Catalog={databaseName};Integrated Security=true;Application Name=BoilerplateGenerator";

            // Provide the query string with a parameter placeholder.
            var queryString =
                "SELECT " +
                    "col.[COLUMN_NAME] " +
                    ",col.[IS_NULLABLE] " +
                    ",CASE WHEN kcu.COLUMN_NAME = col.COLUMN_NAME AND tc.CONSTRAINT_TYPE = 'PRIMARY KEY' THEN 'YES' " +
                    "ELSE 'NO' " +
                    "END AS \"PrimaryKey\" " +
                    ",col.[DATA_TYPE] " +
                    ",col.[CHARACTER_MAXIMUM_LENGTH] " +
                    ",col.[NUMERIC_PRECISION] " +
                    ",col.[NUMERIC_SCALE] " +
                $"FROM [{databaseName}].[INFORMATION_SCHEMA].[COLUMNS] col " +
                    $"LEFT OUTER JOIN[{databaseName}].[INFORMATION_SCHEMA].[TABLE_CONSTRAINTS] tc on tc.TABLE_CATALOG = col.TABLE_CATALOG AND tc.TABLE_SCHEMA = col.TABLE_SCHEMA AND tc.TABLE_NAME = col.TABLE_NAME AND tc.CONSTRAINT_TYPE = 'PRIMARY KEY' " +
                    $"LEFT OUTER JOIN[{databaseName}].[INFORMATION_SCHEMA].[KEY_COLUMN_USAGE] kcu on kcu.TABLE_CATALOG = col.TABLE_CATALOG AND kcu.TABLE_SCHEMA = col.TABLE_SCHEMA AND kcu.TABLE_NAME = col.TABLE_NAME AND kcu.COLUMN_NAME = col.COLUMN_NAME AND kcu.CONSTRAINT_NAME = tc.CONSTRAINT_NAME " +
                $"WHERE col.TABLE_CATALOG = '{databaseName}' AND col.TABLE_SCHEMA = '{schemaName}' AND col.TABLE_NAME = '{tableName}' " +
                "ORDER BY col.ORDINAL_POSITION;";

            var fields = new List<Field>();

            using (var connection = new SqlConnection(connectionString))
            {
                var command = new SqlCommand(queryString, connection);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    var extractedField = ExtractField(reader);
                    if (!extractedField.SqlColumnName.Equals("version", StringComparison.InvariantCultureIgnoreCase))
                    {
                        fields.Add(extractedField);
                    }
                }

                reader.Close();
            }

            return fields;
        }

        private static Field ExtractField(SqlDataReader reader)
        {
            var newField = new Field
            {
                SqlColumnName = (string)reader[0],
                SnowflakeColumnName = ConvertSqlNameToSnowflakeName((string)reader[0], ConvertToType((string)reader[3])),
                IsNullable = ConvertToBool((string)reader[1]),
                IsPrimaryKey = ConvertToBool((string)reader[2]),
                Type = ConvertToType((string)reader[3]),
                StringMaxLength = ConvertToInt(reader[4]),
                NumericPrecision = ConvertToInt(reader[5]),
                NumericScale = ConvertToInt(reader[6])
            };

            return newField;
        }

        public static string ConvertSqlNameToSnowflakeName(string columnName, FieldType fieldType)
        {
            var separatedName = Regex.Replace(columnName, "([a-z](?=[A-Z]|[0-9])|[A-Z](?=[A-Z][a-z]|[0-9])|[0-9](?=[^0-9]))", "$1_");
            var lower = separatedName.ToLower();
            if (fieldType == FieldType.Boolean && !lower.StartsWith("is_"))
            {
                lower = $"is_{lower}";
            }

            if (lower.Contains("net_suite"))
            {
                lower = lower.Replace("net_suite", "netsuite");
            }

            return lower;
        }

        private static int ConvertToInt(object p0)
        {
            if (p0 is DBNull) return 0;

            return Convert.ToInt32(p0);
        }

        private static FieldType ConvertToType(string typeString)
        {
            typeString = typeString.ToLower();

            switch (typeString)
            {
                case "varchar":
                case "nvarchar":
                    return FieldType.String;

                case "int":
                    return FieldType.Int;

                case "datetime2":
                    return FieldType.DateTime;

                case "decimal":
                    return FieldType.Decimal;

                case "bit":
                    return FieldType.Boolean;

                default:
                    throw new Exception($"Unrecognized type {typeString}");
            }
        }

        private static bool ConvertToBool(string boolString)
        {
            return boolString.Equals("YES", StringComparison.InvariantCultureIgnoreCase);
        }
    }
}
