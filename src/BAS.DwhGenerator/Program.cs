﻿using System;
using System.Collections.Generic;
using System.IO;
using LoadingDbDeploy = BAS.DwhGenerator.Templates.Schema.loading_db.deploy;
using LoadingDbRevert = BAS.DwhGenerator.Templates.Schema.loading_db.revert;
using LoadingDbVerify = BAS.DwhGenerator.Templates.Schema.loading_db.verify;
using StagingDbDeploy = BAS.DwhGenerator.Templates.Schema.staging_db.deploy;
using StagingDbRevert = BAS.DwhGenerator.Templates.Schema.staging_db.revert;
using StagingDbVerify = BAS.DwhGenerator.Templates.Schema.staging_db.verify;
using DatavaultDbDeployHub = BAS.DwhGenerator.Templates.Schema.datavault_db.deploy_hub;
using DatavaultDbDeploySat = BAS.DwhGenerator.Templates.Schema.datavault_db.deploy_sat;
using DatavaultDbRevertHub = BAS.DwhGenerator.Templates.Schema.datavault_db.revert_hub;
using DatavaultDbRevertSat = BAS.DwhGenerator.Templates.Schema.datavault_db.revert_sat;
using DatavaultDbVerifyHub = BAS.DwhGenerator.Templates.Schema.datavault_db.verify_hub;
using DatavaultDbVerifySat = BAS.DwhGenerator.Templates.Schema.datavault_db.verify_sat;

using ExtractDataCode = BAS.DwhGenerator.Templates.Dag.utils.extract;
using ExtractDataSql = BAS.DwhGenerator.Templates.Dag.sql.extract.extract;
using LoadingDbLoadDataSql = BAS.DwhGenerator.Templates.Dag.sql.stage.load;
using LoadingDbDeleteDataSql = BAS.DwhGenerator.Templates.Dag.sql.stage.delete;
using StagingDbTransformDataSql = BAS.DwhGenerator.Templates.Dag.sql.transform.transform;
using StagingDbDeleteDataSql = BAS.DwhGenerator.Templates.Dag.sql.transform.delete;
using DatavaultDbLoadHubSql = BAS.DwhGenerator.Templates.Dag.sql.datavault.load_hub;
using DatavaultDbLoadSatSql = BAS.DwhGenerator.Templates.Dag.sql.datavault.load_sat;

using ETLCode = BAS.DwhGenerator.Templates.Dag.etl;

namespace BAS.DwhGenerator
{
    class Program
    {
        static void Main()
        {
            var outputFolder = "C:\\PTemp\\DwhGenerated";
            var databaseName = "OrderLifecycle";
            var sqlSchemaName = "dbo";
            var sqlTableName = "RevenueDistribution";
            var dateSourceTableName = "RevenueDistributionEvent";
            var dateSourceColumnName = "Date";
            var fields = FieldExtractor.ExtractFields(databaseName, sqlSchemaName, sqlTableName);

            var snowflakeTableName = FieldExtractor.ConvertSqlNameToSnowflakeName(sqlTableName, FieldType.String);

            var transformers = new List<ITransformer>
            {
                // Create schema.
                new LoadingDbDeploy(databaseName, sqlTableName, snowflakeTableName, fields),
                new LoadingDbRevert(databaseName, sqlTableName, snowflakeTableName, fields),
                new LoadingDbVerify(databaseName, sqlTableName, snowflakeTableName, fields),
                new StagingDbDeploy(databaseName, sqlTableName, snowflakeTableName, fields),
                new StagingDbRevert(databaseName, sqlTableName, snowflakeTableName, fields),
                new StagingDbVerify(databaseName, sqlTableName, snowflakeTableName, fields),
                new DatavaultDbDeployHub(databaseName, sqlTableName, snowflakeTableName, fields),
                new DatavaultDbDeploySat(databaseName, sqlTableName, snowflakeTableName, fields),
                new DatavaultDbRevertHub(databaseName, sqlTableName, snowflakeTableName, fields),
                new DatavaultDbRevertSat(databaseName, sqlTableName, snowflakeTableName, fields),
                new DatavaultDbVerifyHub(databaseName, sqlTableName, snowflakeTableName, fields),
                new DatavaultDbVerifySat(databaseName, sqlTableName, snowflakeTableName, fields),

                // Extract data from SQL
                new ExtractDataCode(databaseName, sqlTableName, snowflakeTableName, fields),
                new ExtractDataSql(databaseName, sqlSchemaName, sqlTableName, dateSourceTableName, dateSourceColumnName, snowflakeTableName, fields),

                // Load raw data into Snowflake loading db.
                new LoadingDbLoadDataSql(snowflakeTableName, fields),
                new LoadingDbDeleteDataSql(snowflakeTableName),

                // Transform data into the staging db.
                new StagingDbTransformDataSql(snowflakeTableName, fields),
                new StagingDbDeleteDataSql(snowflakeTableName),

                // Load data into the datavault db.
                new DatavaultDbLoadHubSql(snowflakeTableName, fields),
                new DatavaultDbLoadSatSql(snowflakeTableName, fields),

                // Create ETL.
                new ETLCode(databaseName, sqlTableName, snowflakeTableName, fields)
            };

            foreach (var transformer in transformers)
            {
                Generate(transformer, outputFolder, snowflakeTableName);
            }
        }

        private static void Generate(ITransformer transformer, string outputFolder, string snowflakeTableName)
        {
            var transformedText = transformer.TransformText().Split(Environment.NewLine);

            var filePathName = Path.Join(outputFolder, transformer.OutputFileName(snowflakeTableName));
            
            if (!Directory.Exists(Path.GetDirectoryName(filePathName)))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(filePathName));
            }

            File.WriteAllText(filePathName, string.Join("\n", transformedText) + "\n");
        }
    }
}