﻿namespace BAS.DwhGenerator
{
    public enum FieldType
    {
        String,
        Int,
        DateTime,
        Decimal,
        Boolean
    }

    public class Field
    {
        public string SqlColumnName { get; set; }
        public string SnowflakeColumnName { get; set; }
        public bool IsNullable { get; set; }
        public bool IsPrimaryKey { get; set; }
        public FieldType Type { get; set; }
        public int StringMaxLength { get; set; }
        public int NumericPrecision { get; set; }
        public int NumericScale { get; set; }
    }
}
