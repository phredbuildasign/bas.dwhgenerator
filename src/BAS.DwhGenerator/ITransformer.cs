﻿namespace BAS.DwhGenerator
{
    interface ITransformer
    {
        string TransformText();

        string OutputFileName(string snowflakeTableName);
    }
}
