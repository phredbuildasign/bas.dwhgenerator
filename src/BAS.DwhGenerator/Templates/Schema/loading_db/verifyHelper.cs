﻿using System.Collections.Generic;

namespace BAS.DwhGenerator.Templates.Schema.loading_db
{
    public partial class verify : ITransformer
    {
        private string DatabaseName { get; }
        private string SqlTableName { get; }
        private string SnowflakeTableName { get; }
        private IList<Field> Fields { get; }

        public verify(
            string databaseName,
            string sqlTableName,
            string snowflakeTableName,
            IList<Field> fields)
        {
            DatabaseName = databaseName;
            SqlTableName = sqlTableName;
            SnowflakeTableName = snowflakeTableName;
            Fields = fields;
        }

        public string OutputFileName(string snowflakeTableName)
        {
            return $"schema\\loading_db\\verify\\table_{snowflakeTableName}_create.sql";
        }
    }
}
