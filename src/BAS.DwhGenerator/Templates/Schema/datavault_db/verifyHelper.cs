﻿using System.Collections.Generic;

namespace BAS.DwhGenerator.Templates.Schema.datavault_db
{
    public partial class verify_hub : ITransformer
    {
        private string DatabaseName { get; }
        private string SqlTableName { get; }
        private string SnowflakeTableName { get; }
        private IList<Field> Fields { get; }

        public verify_hub(
            string databaseName,
            string sqlTableName,
            string snowflakeTableName,
            IList<Field> fields)
        {
            DatabaseName = databaseName;
            SqlTableName = sqlTableName;
            SnowflakeTableName = snowflakeTableName;
            Fields = fields;
        }

        public string OutputFileName(string snowflakeTableName)
        {
            return $"schema\\datavault_db\\verify\\hub_{snowflakeTableName}_create.sql";
        }
    }

    public partial class verify_sat : ITransformer
    {
        private string DatabaseName { get; }
        private string SqlTableName { get; }
        private string SnowflakeTableName { get; }
        private IList<Field> Fields { get; }

        public verify_sat(
            string databaseName,
            string sqlTableName,
            string snowflakeTableName,
            IList<Field> fields)
        {
            DatabaseName = databaseName;
            SqlTableName = sqlTableName;
            SnowflakeTableName = snowflakeTableName;
            Fields = fields;
        }

        public string OutputFileName(string snowflakeTableName)
        {
            return $"schema\\datavault_db\\verify\\sat_{snowflakeTableName}_detail_create.sql";
        }
    }
}
