﻿using System.Collections.Generic;

namespace BAS.DwhGenerator.Templates.Schema.staging_db
{
    public partial class revert : ITransformer
    {
        private string DatabaseName { get; }
        private string SqlTableName { get; }
        private string SnowflakeTableName { get; }
        private IList<Field> Fields { get; }

        public revert(
            string databaseName,
            string sqlTableName,
            string snowflakeTableName,
            IList<Field> fields)
        {
            DatabaseName = databaseName;
            SqlTableName = sqlTableName;
            SnowflakeTableName = snowflakeTableName;
            Fields = fields;
        }

        public string OutputFileName(string snowflakeTableName)
        {
            return $"schema\\staging_db\\revert\\table_{snowflakeTableName}_create.sql";
        }
    }
}
