﻿using System.Collections.Generic;

namespace BAS.DwhGenerator.Templates.Dag.utils
{
    public partial class extract : ITransformer
    {
        private string DatabaseName { get; }
        private string SqlTableName { get; }
        private string SnowflakeTableName { get; }
        private IList<Field> Fields { get; }

        public extract(
            string databaseName,
            string sqlTableName,
            string snowflakeTableName,
            IList<Field> fields)
        {
            DatabaseName = databaseName;
            SqlTableName = sqlTableName;
            SnowflakeTableName = snowflakeTableName;
            Fields = fields;
        }

        public string OutputFileName(string snowflakeTableName)
        {
            return $"dag\\utils\\extract_{snowflakeTableName}_data.py";
        }
    }
}
