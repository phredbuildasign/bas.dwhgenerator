﻿using System.Collections.Generic;

namespace BAS.DwhGenerator.Templates.Dag
{
    public partial class etl : ITransformer
    {
        private string DatabaseName { get; }
        private string UnderscoreDatabaseName { get; }
        private string SqlTableName { get; }
        private string SnowflakeTableName { get; }
        private IList<Field> Fields { get; }

        public etl(
            string databaseName,
            string sqlTableName,
            string snowflakeTableName,
            IList<Field> fields)
        {
            DatabaseName = databaseName.ToLower();
            UnderscoreDatabaseName = FieldExtractor.ConvertSqlNameToSnowflakeName(databaseName, FieldType.String);
            SqlTableName = sqlTableName;
            SnowflakeTableName = snowflakeTableName;
            Fields = fields;
        }

        public string OutputFileName(string snowflakeTableName)
        {
            return $"dag\\etl_{DatabaseName.ToLower()}_{snowflakeTableName}.py";
        }
    }
}
