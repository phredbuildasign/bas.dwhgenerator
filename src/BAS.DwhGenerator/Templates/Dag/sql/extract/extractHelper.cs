﻿using System.Collections.Generic;

namespace BAS.DwhGenerator.Templates.Dag.sql.extract
{
    public partial class extract : ITransformer
    {
        private string DatabaseName { get; }
        private string SqlSchemaName { get; }
        private string SqlTableName { get; }
        private string DateSourceTableName { get; }
        private string DateSourceColumnName { get; }
        private string SnowflakeTableName { get; }
        private IList<Field> Fields { get; }

        public extract(
            string databaseName,
            string sqlSchemaName,
            string sqlTableName,
            string dateSourceTableName,
            string dateSourceColumnName,
            string snowflakeTableName,
            IList<Field> fields)
        {
            DatabaseName = databaseName;
            SqlSchemaName = sqlSchemaName;
            SqlTableName = sqlTableName;
            DateSourceTableName = dateSourceTableName;
            DateSourceColumnName = dateSourceColumnName;
            SnowflakeTableName = snowflakeTableName;
            Fields = fields;
        }

        public string OutputFileName(string snowflakeTableName)
        {
            return $"dag\\sql\\extract\\extract_{snowflakeTableName}.sql";
        }
    }
}
