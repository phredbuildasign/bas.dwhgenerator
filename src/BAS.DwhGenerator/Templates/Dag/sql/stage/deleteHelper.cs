﻿using System.Collections.Generic;

namespace BAS.DwhGenerator.Templates.Dag.sql.stage
{
    public partial class delete : ITransformer
    {
        private string SnowflakeTableName { get; }

        public delete(string snowflakeTableName)
        {
            SnowflakeTableName = snowflakeTableName;
        }

        public string OutputFileName(string snowflakeTableName)
        {
            return $"dag\\sql\\stage\\delete_{snowflakeTableName}.sql";
        }
    }
}
