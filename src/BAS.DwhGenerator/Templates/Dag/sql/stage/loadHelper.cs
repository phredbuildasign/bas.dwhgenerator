﻿using System.Collections.Generic;

namespace BAS.DwhGenerator.Templates.Dag.sql.stage
{
    public partial class load : ITransformer
    {
        private string SnowflakeTableName { get; }
        private IList<Field> Fields { get; }

        public load(
            string snowflakeTableName,
            IList<Field> fields)
        {
            SnowflakeTableName = snowflakeTableName;
            Fields = fields;
        }

        public string OutputFileName(string snowflakeTableName)
        {
            return $"dag\\sql\\stage\\load_{snowflakeTableName}.sql";
        }
    }
}
