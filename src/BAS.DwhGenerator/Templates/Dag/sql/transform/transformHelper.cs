﻿using System.Collections.Generic;

namespace BAS.DwhGenerator.Templates.Dag.sql.transform
{
    public partial class transform : ITransformer
    {
        private string SnowflakeTableName { get; }
        private IList<Field> Fields { get; }

        public transform(
            string snowflakeTableName,
            IList<Field> fields)
        {
            SnowflakeTableName = snowflakeTableName;
            Fields = fields;
        }

        public string OutputFileName(string snowflakeTableName)
        {
            return $"dag\\sql\\transform\\transform_{snowflakeTableName}.sql";
        }
    }
}
