﻿namespace BAS.DwhGenerator.Templates.Dag.sql.transform
{
    public partial class delete : ITransformer
    {
        private string SnowflakeTableName { get; }

        public delete(string snowflakeTableName)
        {
            SnowflakeTableName = snowflakeTableName;
        }

        public string OutputFileName(string snowflakeTableName)
        {
            return $"dag\\sql\\transform\\delete_staged_{snowflakeTableName}.sql";
        }
    }
}
