﻿using System.Collections.Generic;

namespace BAS.DwhGenerator.Templates.Dag.sql.datavault
{
    public partial class load_hub : ITransformer
    {
        private string SnowflakeTableName { get; }
        private IList<Field> Fields { get; }

        public load_hub(
            string snowflakeTableName,
            IList<Field> fields)
        {
            SnowflakeTableName = snowflakeTableName;
            Fields = fields;
        }

        public string OutputFileName(string snowflakeTableName)
        {
            return $"dag\\sql\\datavault\\load_hub_{snowflakeTableName}.sql";
        }
    }
}